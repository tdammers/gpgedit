#!/usr/bin/env python
import sys
import os
import os.path
import subprocess
import re
import tempfile

# A helper script that allows editing gpg-encrypted files in your editor of
# choice.

editor = os.getenv('EDITOR')
gpg = '/usr/bin/gpg'
version = '0.2'

def get_my_identities():
    global gpg
    listing = subprocess.check_output([gpg, "-K"])
    idents = []

    for l in listing.split('\n'):
        m = re.match('^uid\s*(\S.*)$', l)
        if m:
            idents.append(m.group(1))
    return idents

def parse_recipients(info):
    recipients = []
    for l in info.split('\n'):
        m = re.search('encrypted with (\d+)-bit (\S+) key, ID ([0-9A-F]+),', l)
        if m:
            recipients.append({
                'algorithm': m.group(2),
                'keysize': m.group(1),
                'id': m.group(3)})
    return recipients

def decrypt_file(fn):
    p = subprocess.Popen(
        [gpg, "--decrypt", fn],
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE)
    (decrypted, info) = p.communicate()
    recipients = parse_recipients(info)
    return (decrypted, recipients)

def show_file():
    fn = sys.argv[2]
    (decrypted, recipients) = decrypt_file(fn)
    print(decrypted)

def show_help():
    progname = os.path.basename(sys.argv[0])
    helpscreen = \
        u"""{0} - read and update gpg-encrypted files in your favorite editor.
Syntax: {0} command [args...]
Commands:
    help          display this help screen
    version
    --version
    -v            display version information
    show FILE     print the contents of the encrypted FILE on
                  stdout (this is equivalent to gpg --decrypt FILE)
    recode FILE   decrypt the GPG file and re-encrypt it unchanged.
    add-recipients FILE KEYIDS...
                  decrypt the GPG file, re-encrypt it unchanged but
                  add KEYIDS to the recipients.
    edit FILE     decrypt the GPG file, edit it in your favorite
                  editor ($EDITOR), and re-encrypt it.
""".format(progname)
    sys.stderr.write(helpscreen)

def show_version():
    sys.stderr.write(
        u"""gpgedit version {0}
Copyright (c) 2013-2014 by Tobias Dammers.

This is free software; see the enclosed LICENSE for details.
""".format(version))

def edit_file(
        skip_editing=False,
        keep_recipients=True,
        extra_recipients=None):
    fne = sys.argv[2]
    (decrypted, recipients) = decrypt_file(fne)
    (h, fn) = tempfile.mkstemp()
    try:
        f = open(fn, 'w')
        f.write(decrypted)
        f.close()

        if skip_editing:
            retcode = 0
        else:
            retcode = subprocess.call([editor, fn])

        if retcode != 0:
            raise Exception(
                u"Editor {0} returned non-zero exit code {1}".format(editor, retcode))

        args = [gpg]
        if keep_recipients:
            dest_recipients = recipients
        else:
            dest_recipients = []
        if extra_recipients is not None:
            dest_recipients = \
                dest_recipients + \
                [ {'id': i} for i in extra_recipients ]

        for r in dest_recipients:
            args.append('--recipient')
            args.append(r['id'])
        args.append('--yes')
        args.append('--compression-algo')
        args.append('none')
        args.append('--output')
        args.append(fne)
        args.append('--encrypt')
        args.append(fn)
        print args
        subprocess.call(args)
    finally:
        try:
            subprocess.call(['shred', fn])
        except Exception, e:
            sys.stderr.write("shred step failed: " + e.message + "\n")
        finally:
            os.unlink(fn)

def recode_file():
    edit_file(True, False)

def add_recipients(*recipients):
    edit_file(True, True, recipients)

actions = {
    'help': show_help,
    'version': show_version,
    '-v': show_version,
    '--version': show_version,
    'show': show_file,
    'edit': edit_file,
    'add-recipients': add_recipients,
    'recode': recode_file
}

def main():
    try:
        action_key = sys.argv[1]
        if not action_key in actions:
            raise Exception('Please specify a valid action: ' +
                    ', '.join(actions.keys()))
        action = actions[action_key]
        extra_args = sys.argv[3:]
        action(*extra_args)
    except Exception, e:
        sys.stderr.write(e.message + "\n")
        sys.exit(-1)

if __name__ == '__main__':
    main()
